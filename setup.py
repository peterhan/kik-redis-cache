# -*- coding: utf-8 -*-
from __future__ import absolute_import
from setuptools import setup, find_packages
from os import path
from kik_redis_cache import __version__


try:
    with open(path.join(path.dirname(__file__), 'README.md')) as f:
        long_description = f.read()
except Exception:
    # XXX: Intentional pokemon catch to prevent this read from breaking setup.py
    long_description = None

setup(
    name='kik-redis-cache',
    version=__version__,
    author='Peter Han',
    author_email='git@peterhan.me',
    description='Unofficial Kik client with Redis cache',
    long_description=long_description,
    url='https://gitlab.com/peterhan/kik-redis-cache',
    packages=find_packages(exclude=['tests']),
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Communications :: Chat',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    install_requires=[
        'kik_unofficial',
        'redis',
    ],
    dependency_links=[
        'git+https://github.com/tomer8007/kik-bot-api-unofficial.git@new#egg=kik_unofficial-0',
    ],
    tests_require=[
        'pytest',
    ],
    test_suite='tests',
    python_requires='~=3.6',
)
